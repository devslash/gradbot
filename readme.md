# GradApp - Chatbot

GradApp is a `node` app. To run you should 

1. npm install 
1. npm start

If you want to run it directly you need to run bin/www through node via `node bin/www`

# Dependencies
This project uses `Express.js` for its routing and web-server applications. 
To learn more, go to http://expressjs.com/

All actions for hipchat run through the `/api` route. This can be found at `routes/api.js`. 

If you want to add a command you can do so in the `api.js` post request. 

To add this to a hipchat room you can use the server that exists at `api.devslash.net/api` or run it yourself. 
