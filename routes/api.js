var express = require('express');
var router = express.Router();

const seconds = 1000;
const minutes = 60 * seconds;
const hours = 60 * minutes;
const days = 24 * hours;

const Times = {
  DAYS: days,
  GSTYLE: seconds * 252,
  BMARRIAGE: days * 55
};

function getMillisecondsToDate() {
  var dateTo = new Date('01/18/2016 09:00');
  return dateTo - Date.now();
}

function getDate(time, fixed) {
  return (getMillisecondsToDate() / time).toFixed(fixed);
}

/* GET users listing. */
router.post('/', function(req, res, next) {
  var data = req.body;
  var message = data.item.message.message;
  var respObj = {notify: false, message_format: 'html'};

  var command = 'default';
  if(message.split(' ').length > 1) {
    command = message.split(' ')[1];
  }

  if(command === "days") {
    respObj.message = "You've still got " + getDate(Times.DAYS, 2) + " days left";
  }
  else if(command === "countdown") {
    respObj.message = "<b>Countdown till 18th Jan 2016 at 9:00am</b>";
    respObj.message += "<ul>";
    respObj.message += "<li>" + getDate(Times.DAYS, 0) + " days left! </li>";
    respObj.message += "<li>Go listen to <a href='https://www.youtube.com/watch?v=9bZkp7q19f0'>Gangnam Style</a> " + getDate(Times.GSTYLE, 0) + " times!</li>";
    respObj.message += "<li>You can download " + (getDate(seconds, 5) * 0.000556111335754394).toFixed(3)
        + "GB on the average Australian internet</li>";
    respObj.message += "</ul>";
  } else {
    respObj.message = "What did you mean? You can send the following commands:" +
        "<ul>" +
        "<li><i>days</i> - days left</li>" +
        "<li><i>countdown</i> - more stats!</li>" +
        "</ul>";
  }

  //data.item.message.
  res.json(respObj);
});

module.exports = router;
